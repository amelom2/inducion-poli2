{
	"title": "Employee Health and Wellness (Sample Course)",
	"id": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
	"originalId": null,
	"author": "auth0|5a1070c1f391697c5b5a4997",
	"selectedAuthorId": "",
	"color": "#FF631E",
	"navigationMode": "free",
	"sharePassword": "",
	"description": "<p>We’re committed to creating a work environment that promotes employee health and wellness. This course is the first step in raising awareness and rollout of our new workplace health program, which includes promoting activities or policies that encourage and support healthy behaviors in the workplace.</p><p><br></p><p>Developing a thoughtful workplace health program has the potential to transform our organization. We hope it increases employee wellness, work satisfaction, and productivity. In this course, we’ll introduce some wellness concepts to kick off our wellness journey. Let’s get started!</p>",
	"shareId": "uxyDYJANb8Ax-zbd0LjFft9j1-5HO3fZ",
	"copyOf": "",
	"order": "1511038719605",
	"fonts": {
		"ui": {
			"font": "Lato"
		},
		"body": {
			"font": "Merriweather"
		},
		"headings": {
			"font": "Lato"
		}
	},
	"media": {},
	"coverImage": {},
	"lessons": [{
		"id": "andres_cero",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "5 Weeks to a Healthier You",
		"description": "<p>Improving your health is easier than you might think. In this lesson, you'll learn simple, actionable steps to building a more healthy you. While we cover just five weeks, it's important to make these tips a habit if you want them to have a lasting impact on your physical and emotional wellbeing.</p>",
		"type": "timeline",
		"icon": "Interaction",
		"items": [{
			"id": 1,
			"date": "Week 1",
			"title": "Create a healthy meal plan",
			"description": "<p>Plan your meals in advance to make sure you have a nutritious, well-balanced diet. Include foods from each of the food groups, minimize salt and processed foods, and rely heavily on nutrient-rich fruits, vegetables, and whole grains.</p>",
			"linePosition": "19.288079470198678%"
		}, {
			"id": 2,
			"date": "Week 2",
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/running-streets.jpg",
					"src": "1nYKqqF9hjrxN5s7_running-streets.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/running-streets-thumb.jpg",
					"originalUrl": "running-streets.jpg"
				}
			},
			"title": "Make exercise a habit",
			"description": "<p>Exercise is fundamental to good physical and mental health and it’s easier to make it a habit than you might think. The most important thing to remember is that you need to start small. Choose an exercise that you’d be willing to do even if you’re tired or lacking in motivation. Then, write down when, where, and what you’ll do. Research shows that when you set your exercise intention specifically, you’re more likely to do it.</p>",
			"linePosition": "34.35430463576159%"
		}, {
			"id": 3,
			"date": "Week 3",
			"media": {
				"audio": {
					"key": "yuDZiQenGudWuTO__friends.mp3",
					"src": "https://cdn.articulate.com/assets/rise/assets/sample-course/friends.mp3",
					"time": 9.28798185941043,
					"originalUrl": "recording.mp3"
				}
			},
			"title": "Open up to friends",
			"description": "<p>People who connect emotionally with friends and loved ones regularly lead longer, happier, and healthier lives. One 2010 study even found that people who have more substantive conversations than small talk have the highest levels of wellbeing. Over the next few weeks, try doing a fun activity or seeking support from your friends.</p>",
			"linePosition": "49.420529801324506%"
		}, {
			"id": 4,
			"date": "Week 4",
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/sunset.jpg",
					"src": "FMjgjTFI4RCCKlg6_sunset.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/sunset-thumb.jpg",
					"originalUrl": "sunset.jpg"
				}
			},
			"title": "Make yourself a priority",
			"description": "<p>Life and work can get so busy that we can forget to take care of ourselves. Sometimes we don’t even realize we’ve been neglectful until we’re worn out and sick. Do you get enough sleep? Are you creating time for fun? When did you last do an activity that brings you peace or joy? Write down the things you need to do to take care of yourself. Then try doing at least one every day.&nbsp;</p>",
			"linePosition": "64.98344370860927%"
		}, {
			"id": 5,
			"date": "Week 5",
			"title": "Tackle stress head on",
			"description": "<p>We all experience stress in our lives. But not all ways of coping with stress are equal. Negative coping mechanisms include drinking, smoking, emotional eating, and ignoring or avoiding stress. Positive ways to counteract stress include meditation, breathing, yoga, exercise, play, prayer, and positive self-talk. Learn to recognize signs that you’re feeling stress and adopt practices that help you positively counteract it.</p>",
			"linePosition": "81.12582781456953%"
		}],
		"media": {},
		"piles": [],
		"settings": {},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}, {
		"id": "yS3lDAf1IQYXP0WobvennVUO28Do-mY3",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "Creating Healthy Meals",
		"description": "Click on the markers to learn the health benefits of each ingredient. You'll also learn tips for incorporating these foods into your daily diet.",
		"type": "map",
		"icon": "Interaction",
		"items": [{
			"x": "25.98039215686275",
			"y": "51.49441683197005",
			"id": 2,
			"icon": "01",
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/avocado.jpg",
					"src": "SInZ0NOajBjmV3W0_avocado.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/avocado-thumb.jpg",
					"originalUrl": "avocado.jpg"
				}
			},
			"title": "Avocado",
			"hasMedia": false,
			"isActive": false,
			"description": "<p>Avocado is on everything these days and for good reason. Not only do diners appreciate this fruit’s delicious flavor and smooth, buttery texture, they also approve of its health benefits. Avocados are packed with fiber, potassium, and heart-healthy fats—all nutrients many of us could stand to get more of in our diets. Try avocado in your next salad or on a piece of toast with salt and pepper.</p>"
		}, {
			"x": "29.570501207729468",
			"y": "77.6392740451937",
			"id": 3,
			"icon": "01",
			"title": "Whole Grains",
			"isActive": false,
			"description": "<p>Whole grains come in many textures and flavors. Barley, couscous, millet, popcorn, and quinoa are all examples of whole grains. They’re packed with protein, fiber, and other nutrients your body loves. Studies suggest that whole grains can help reduce your risk of heart disease, type 2 diabetes, and some forms of cancer. Anywhere from one to three servings of whole grains per day can help you reap these health benefits. So eat up! Serve them as a side or add them to soups, stews, and other dishes.</p>"
		}, {
			"x": "58.92156862745098",
			"y": "34.23451534535404",
			"id": 4,
			"icon": "01",
			"media": {
				"embed": {
					"src": "//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Fwn3xBDqdqpc%3Ffeature%3Doembed&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dwn3xBDqdqpc&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fwn3xBDqdqpc%2Fhqdefault.jpg&key=5cbac80a25df462a99e58eccd801acc0&type=text%2Fhtml&schema=youtube",
					"type": "video",
					"title": "Easy Fruit Salad 3 Delicious Ways",
					"favicon": "https://s.ytimg.com/yts/img/favicon-vflz7uhzw.ico",
					"provider": "YouTube",
					"thumbnail": "https://i.ytimg.com/vi/wn3xBDqdqpc/hqdefault.jpg",
					"description": "SUBSCRIBE to The Domestic Geek here: http://bit.ly/1dn24vP *NEW* Meal Prep Made Easy eBook | Menus 13 -18: http://bit.ly/MealPrepMadeEasy3 eBook Bundle | All three Meal Prep Made Easy eBooks for only $12.99: http://bit.ly/MealPrepBundle Today we're making 3 fresh fruit salads that are easy to make and perfect for summer!",
					"originalUrl": "https://www.youtube.com/watch?v=wn3xBDqdqpc",
					"providerUrl": "https://www.youtube.com/"
				}
			},
			"title": "Mixed Fruit",
			"hasMedia": false,
			"isActive": false,
			"description": "<p>Eating a colorful array of fruits and veggies is crucial to good health. Researchers recommend eating 5 to 10 servings every day. If that seems like a lot, consider this: The antioxidants in fruits and vegetables work even harder when they’re in the presence of other antioxidants. So the more fruits and veggies you eat, the more effective these nutrients become!</p>"
		}, {
			"x": "24.919359128456044",
			"y": "14.84566705568994",
			"id": 6,
			"icon": "01",
			"title": "Fresh Juice",
			"hasMedia": false,
			"isActive": false,
			"description": "<p>You can pour a lot of health benefits into a small glass! Studies suggest that freshly squeezed juice from citrus fruits, such as oranges and grapefruit, boost immunity, detoxify the body, and more—all while being simply delicious. But did you know that you can also juice your greens? That’s right. Beverages made with herbs, grasses, and vegetables are popular for their unique flavors and myriad health benefits.<br></p>"
		}, {
			"x": "75.07236055790639",
			"y": "18.187201817307468",
			"id": 7,
			"icon": "01",
			"title": "Grapes",
			"hasMedia": false,
			"isActive": false,
			"description": "<p>Grapes are portable, crunchy, and delicious. Plus, they’re great for you.<br></p><p><br></p>"
		}],
		"media": {
			"image": {
				"key": "assets/rise/assets/sample-course/breakfast.jpg",
				"src": "4etzWuUKbDCPolau_breakfast.jpg",
				"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/breakfast-thumb.jpg",
				"originalUrl": "breakfast.jpg"
			}
		},
		"piles": [],
		"settings": {},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}, {
		"id": "vjG6ZhsNUT-FZVA1KnlNRjX9zLXvulAs",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "Gauging Your Wellness",
		"description": "<p>Many of us don’t pause to reflect on our habits until it’s too late—by then we’re sick, crabby, and overworked. But making a daily note of personal wellness factors like sleep quality, self esteem, and stress levels can help us spot helpful patterns and develop behaviors that make us happier and healthier. For example, does eating certain foods make you feel sluggish? Does drinking caffeine after a certain point in the day keep you up at night? Recording these observations in a journal or a note in your phone is a quick and easy way to get them down so you can reflect on them later.</p>",
		"type": "process",
		"icon": "Interaction",
		"items": [{
			"id": 1,
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/kayak.jpg",
					"src": "B29j4odqa99pxzrV_kayak.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/kayak-thumb.jpg",
					"originalUrl": "kayak.jpg"
				}
			},
			"title": "Understand Your Physical Being",
			"description": "<p>Physical health is closely tied to mental and emotional health. Taking good care of your body helps you think more clearly and better regulate your feelings.</p><p><br></p><p>So next time you’re feeling overworked, upset, or just not “in the groove,” step away from your desk, close your laptop, and do something physical.</p><p><br></p><p>Taking time to go for a walk or do yoga causes your body to release endorphins, helping you stave off stress and pain. When you settle back into work, you’re likely to notice that your mind feels clearer and calmer than before.</p>"
		}, {
			"id": 2,
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/mountain.jpg",
					"src": "uwliOYX8ivBLI6bP_mountain.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/mountain-thumb.jpg",
					"originalUrl": "mountain.jpg"
				}
			},
			"title": "Get in Touch with Your Emotions",
			"description": "<p>Think about a time you received some incredible news. You may have felt a rush of energy, causing you to dance or jump up and down. You may have noticed that your mind, too, was racing with positive thoughts. </p><p><br></p><p>That’s because emotions impact both our minds and bodies. Feeling good can help us stay healthy, but feeling poorly over long periods of time can be really damaging to our well-being.</p><p><br></p><p>In fact, studies suggest that poor emotional health can lead to a weakened immune system. High levels of stress, anger, and other negative emotions can contribute to high blood pressure, stomach ulcers, or worse. </p><p><br></p><p>So in addition to getting out and moving your body each day, be sure to do activities that help you feel calm, confident, and in control. Engaging in these activities can help you be more resilient to stress and stay healthy.</p>"
		}, {
			"id": 4,
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/traffic.jpg",
					"src": "VWJdV3CPWOK-GDlV_traffic.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/traffic-thumb.jpg",
					"originalUrl": "traffic.jpg"
				}
			},
			"title": "Assess Your Stress",
			"description": "<p>One of the most challenging emotions we face day-to-day is stress. </p><p><br></p><p>People experience stress differently and in response to different triggers. </p><p><br></p><p>We feel some stress very acutely, such as when we’re under a tight deadline or get into an argument with a loved one. </p><p><br></p><p>Other stress feels like background noise in our everyday lives. This type of stress is caused by smaller annoyances, such as being stuck in traffic or hearing an irritating noise.</p><p><br></p><p>All of these factors make stress difficult to study. Many scientists have drafted different stress questionnaires in an attempt to pin down this slippery emotion. And while no test is perfect, scientists have drawn some powerful conclusions about stress. </p><p><br></p><p>Studies show that chronic stress is bad for your health. In addition to annoying physical symptoms, such as headaches, eye tremors, and chest pain, stress can degrade your immune system making existing conditions even worse.</p><p><br></p><p>If you struggle with high stress levels, find ways to engage in calming activities. After all, your health is at stake.</p>"
		}],
		"media": {},
		"piles": [],
		"settings": {},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}, {
		"id": "andres_uno",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "Stretching for Success",
		"description": "<p>It can be tempting to end your workout after finishing the last mile or lifting the last rep. But adding some stretches to your exercise routine can improve your flexibility and decrease your risk of injury. </p><p><br></p><p>Here’s a video demonstrating a basic stretching routine that you can incorporate into your workout today!</p>",
		"type": "video",
		"icon": "Video",
		"items": [],
		"media": {
			"video": {
				"key": "lLZ3CLw4pjXZaBj7_yoga.mp4",
				"src": "https://cdn.articulate.com/assets/rise/assets/sample-course/yoga.mp4",
				"jobId": "1473630308132-DbpqYRx_",
				"poster": "dGZ0R1idyKS7_lk0_yoga-poster.png",
				"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/yoga-thumb.png",
				"originalUrl": "Screen Recording 2016-09-11 at 5.44.31 PM.mp4"
			}
		},
		"piles": [],
		"settings": {},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}, {
		"id": "D4kTOqDbks9JB-TVtDldiubsE2WYUuTM",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "Managing Stress",
		"description": "",
		"type": "blocks",
		"icon": "Article",
		"items": [{
			"id": 1,
			"type": "text",
			"items": [{
				"id": 26,
				"heading": "Understanding Stress",
				"paragraph": "<p>Typically we think of stress as a bad thing, but that's not always the case. At low levels, stress can help you focus and give you the motivation you need to reach your goals. However, when your stress level is too high it stops being helpful and starts being harmful for both your productivity and your health.<br></p><p>It's important to monitor your stress levels and recognize when they're getting too high, so you can take steps to get yourself back on track.</p>"
			}],
			"family": "text",
			"variant": "heading paragraph",
			"settings": {
				"paddingTop": "0",
				"paddingBottom": "3",
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 2,
			"type": "image",
			"items": [{
				"id": 41,
				"media": {
					"image": {
						"key": "assets/rise/assets/sample-course/pondering.jpg",
						"src": "f_-6KxECExP6NAOa_pondering.jpg",
						"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/pondering-thumb.jpg",
						"originalUrl": "pondering.jpg"
					}
				},
				"caption": ""
			}],
			"family": "image",
			"variant": "full",
			"settings": {
				"opacity": 0.2,
				"paddingTop": 3,
				"zoomOnClick": true,
				"opacityColor": "#000000",
				"paddingBottom": 3,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 11,
			"type": "text",
			"items": [{
				"id": 28,
				"heading": "Recognizing Signs of Excess Stress",
				"isNewlyInserted": false
			}],
			"family": "text",
			"variant": "heading",
			"settings": {
				"paddingTop": 3,
				"paddingBottom": 0,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 3,
			"type": "list",
			"items": [{
				"id": 34,
				"number": 1,
				"paragraph": "<p><b>Emotional Symptoms<br></b>When you're overstressed, it's common to become more emotional. You may feel anxious, irritable, apathetic, depressed, or a combination of those feelings.<b><br></b></p>",
				"isNewlyInserted": false
			}, {
				"id": 35,
				"number": 2,
				"paragraph": "<p><b>Physical Symptoms<br></b>Excess stress can cause physical pain such as chronic back or neck pain, muscle tension or headaches, stomach aches, and heart palpitations.</p>",
				"isNewlyInserted": false
			}, {
				"id": 36,
				"number": 3,
				"paragraph": "<p class=''><b>Behavioral Symptoms<br></b>A high level of stress can also impact your behavior. You may find yourself over-eating, under-eating, having more angry outbursts than usual, abusing drugs or alcohol, withdrawing from friends and family, or suddenly becoming interested in risk-taking behavior like gambling or extreme sports. </p>",
				"isNewlyInserted": false
			}],
			"family": "list",
			"variant": "checkboxes",
			"settings": {
				"paddingTop": "3",
				"paddingBottom": "3",
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 12,
			"type": "text",
			"items": [{
				"id": 28,
				"heading": "Coping with Stress"
			}],
			"family": "text",
			"variant": "heading",
			"settings": {
				"paddingTop": 3,
				"paddingBottom": 0,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 5,
			"type": "interactive",
			"items": [{
				"id": 1,
				"media": {
					"image": {
						"key": "assets/rise/assets/sample-course/friends.jpg",
						"src": "fyWd2HP9-gEaiz-F_friends.jpg",
						"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/friends-thumb.jpg",
						"originalUrl": "friends.jpg"
					}
				},
				"title": "TALK TO FAMILY AND FRIENDS",
				"description": "<p>When you're stressed you may feel like pulling away from friends and family and spending time alone, but that's the opposite of what you should be doing. There's no better way to reduce your stress than talking through your feelings with someone who cares about you and can make you feel safe and understood.</p>"
			}, {
				"id": 2,
				"media": {
					"image": {
						"key": "assets/rise/assets/sample-course/kite.jpg",
						"src": "yZ6fU4dyEzv310Jh_kite.jpg",
						"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/kite-thumb.jpg",
						"originalUrl": "kite.jpg"
					}
				},
				"title": "DO SOMETHING FUN",
				"description": "<p>Working long hours when you're on a tight deadline may seem like the only way to get ahead. However, if you never take time to recharge your stress levels will continue to climb and you'll find yourself overwhelmed and unproductive. Make sure to carve out time every day to do something you enjoy.<br></p>"
			}, {
				"id": 3,
				"media": {
					"image": {
						"key": "assets/rise/assets/sample-course/relax.jpg",
						"src": "sOc9c7XHagTgIOxh_relax.jpg",
						"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/relax-thumb.jpg",
						"originalUrl": "relax.jpg"
					}
				},
				"title": "RELAX",
				"description": "<p>Another way to manage your stress is by trying relaxation techniques such as meditation, deep breathing, yoga, or music therapy. There are tons of different options, so if one of them isn't working for you, don't force it. Keep trying different techniques until you find the one that's right for you.<br></p>"
			}],
			"family": "interactive",
			"variant": "tabs",
			"settings": {
				"paddingTop": "3",
				"zoomOnClick": true,
				"paddingBottom": 3,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 7,
			"type": "text",
			"items": [{
				"id": 27,
				"heading": "Takeaway",
				"paragraph": "<p>Recognizing that your stress levels are getting out of hand is the first step in the right direction. It's important to be in tune with your mind and body so you catch onto the signs and symptoms before things get out of control. Then you can apply the coping techniques that work best for you in order to reduce your stress and get back to enjoying life!</p>",
				"isNewlyInserted": false
			}],
			"family": "text",
			"variant": "heading paragraph",
			"settings": {
				"paddingTop": 3,
				"paddingBottom": 3,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 8,
			"type": "quote",
			"items": [{
				"id": 32,
				"name": "<p>Marilu Henner</p>",
				"avatar": {
					"media": {
						"image": {
							"key": "assets/rise/assets/sample-course/quote-avatar.jpg",
							"src": "0q3Lse_Mak4MJtPD_quote-avatar.jpg",
							"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/quote-avatar-thumb.jpg",
							"originalUrl": "avatar.jpg"
						}
					}
				},
				"paragraph": "<p>Being in control of your life and having realistic expectations about your day-to-day challenges are the keys to stress management, which is perhaps the most important ingredient to living a happy, healthy, and rewarding life.</p>",
				"isNewlyInserted": false
			}],
			"family": "quote",
			"variant": "d",
			"settings": {
				"paddingTop": 3,
				"paddingBottom": 3,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}, {
			"id": 10,
			"type": "divider",
			"family": "divider",
			"variant": "spacing divider",
			"settings": {
				"paddingTop": 3,
				"paddingBottom": 3,
				"backgroundColor": "",
				"entranceAnimation": true
			}
		}],
		"media": {},
		"piles": [],
		"settings": {},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}, {
		"id": "JrFg6MvH-RebvboE_X-lzaRO4VVCgEan",
		"originalId": null,
		"author": "auth0|5a1070c1f391697c5b5a4997",
		"selectedAuthorId": "auth0|5a1070c1f391697c5b5a4997",
		"courseId": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E",
		"title": "Wellness Quiz",
		"description": "<p>Now that you’ve explored some aspects of physical, mental, and emotional wellness, it’s time to take a short quiz testing your Wellness IQ. Let’s see what you’ve learned so far!</p>",
		"type": "quiz",
		"icon": "Quiz",
		"items": [{
			"id": 1,
			"type": "MULTIPLE_CHOICE",
			"media": {
				"image": {
					"key": "assets/rise/assets/sample-course/plate.jpg",
					"src": "8zuE8Ad9_ZL9GOHd_plate.jpg",
					"thumbnail": "https://cdn.articulate.com/assets/rise/assets/sample-course/plate-thumb.jpg",
					"originalUrl": "plate.jpg"
				}
			},
			"title": "Myth or Fact - Skipping meals helps you lose weight.",
			"answers": [{
				"id": 1,
				"title": "Myth"
			}, {
				"id": 2,
				"title": "Fact"
			}],
			"correct": 1,
			"feedback": "Studies suggest that eating three or more small meals throughout the day is the key to successfully shedding points and maintaining a healthy weight. Try spreading out your caloric intake over several small meals rather than eating the majority of your daily calories in one sitting."
		}, {
			"id": 2,
			"type": "MULTIPLE_CHOICE",
			"title": "How many times per week should you exercise?",
			"answers": [{
				"id": 1,
				"title": "1-2 times per week"
			}, {
				"id": 2,
				"title": "3-4 times per week"
			}, {
				"id": 3,
				"title": "4-5 times per week"
			}, {
				"id": 4,
				"title": "Every day"
			}],
			"correct": 3,
			"feedback": "While the exact number depends on your fitness goals, exercising four to five times per week is a good frequency for improving overall health. Three of your days should be focused on strength training, with the other two centered around getting in your cardio. And, of course, a couple days of active rest is important, too."
		}],
		"media": {},
		"piles": [],
		"settings": {
			"retryQuiz": true,
			"retryCount": -1,
			"passingScore": 80,
			"revealAnswers": true,
			"shuffleAnswerChoices": false,
			"randomizeQuestionOrder": false
		},
		"duplicatedFromId": "",
		"deleted": false,
		"createdAt": "2017-11-18T17:42:55.122Z",
		"updatedAt": "2017-11-18T17:42:55.122Z",
		"lastUpdatedBy": null,
		"ready": true
	}],
	"jobs": [],
	"labelSetId": "",
	"deleted": false,
	"createdAt": "2017-11-18T17:42:55.113Z",
	"updatedAt": "2017-11-18T20:58:39.605Z",
	"markComplete": false,
	"isDefault": true,
	"ready": true,
	"exportSettings": {
		"title": "Employee Health and Wellness (Sample Course)",
		"format": "zip",
		"quizId": "JrFg6MvH-RebvboE_X-lzaRO4VVCgEan",
		"target": "raw",
		"activeLMS": 4,
		"reporting": "passed-incomplete",
		"identifier": "Bd_C7eZtU7TSXFK3rsk7boc3rgqknX4E_rise",
		"targetName": "Web Only",
		"completeWith": "quiz",
		"quizComplete": true,
		"activeEdition": 0,
		"enableExitCourse": false,
		"completionPercentage": 100
	},
	"reviewId": "",
	"headingTypefaceId": null,
	"bodyTypefaceId": null,
	"uiTypefaceId": null,
	"authors": [{
		"id": "auth0|5a1070c1f391697c5b5a4997",
		"avatar": "Mg8_8LUlDRMqvw4O_small.png",
		"authorName": "andresss melele"
	}],
	"lmsOptions": {
		"enableExitCourse": false
	}
}